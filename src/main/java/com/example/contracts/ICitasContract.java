package com.example.contracts;

import com.example.demo.entity.Citas;

public interface ICitasContract {
	
	public Citas findByTecnico(String fecha, String hora, String tecnico);
	public Citas save(Citas citas);
	public void delete(Long id);
	
}
