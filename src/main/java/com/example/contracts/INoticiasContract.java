package com.example.contracts;

import java.util.List;

import com.example.demo.entity.Noticias;

public interface INoticiasContract {
	
	public List<Noticias>findAll();
	public Noticias save(Noticias noticias);
	public void delete(Long id);
	public Noticias findByTextoAndTitulo(String texto, String titulo);
	public List<Noticias>findSection(int start, int end);
	
}
