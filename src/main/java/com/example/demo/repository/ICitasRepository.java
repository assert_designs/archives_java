package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.Citas;

public interface ICitasRepository extends CrudRepository<Citas, Long> {
	
	@Query("Select c from Citas c where c.fecha=?1 and c.hora=?2 and c.tecnico=?3")
	Citas findByTecnico(String fecha, String hora, String tecnico);
	
}
