package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.Noticias;

public interface INoticiasRepository extends CrudRepository<Noticias, Long> {

	@Query("Select n from Noticias n where n.titulo=?1 and n.texto=?2")
	Noticias findByTextoAndTitulo(String texto, String titulo);

	//String query = "select * from noticias order by id desc limit 0, 3";
	
}
