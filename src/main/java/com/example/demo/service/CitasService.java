package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.contracts.ICitasContract;
import com.example.demo.entity.Citas;
import com.example.demo.repository.ICitasRepository;

@Service
public class CitasService implements ICitasContract {

	@Autowired
	private ICitasRepository citasRepository;
	
	@Override
	public Citas findByTecnico(String fecha, String hora, String tecnico) {
		// TODO Auto-generated method stub
		return citasRepository.findByTecnico(fecha, hora, tecnico);
	}

	@Override
	public Citas save(Citas citas) {
		// TODO Auto-generated method stub
		return citasRepository.save(citas);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		citasRepository.deleteById(id);
	}

}
