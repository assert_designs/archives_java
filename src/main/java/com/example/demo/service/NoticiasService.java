package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.contracts.INoticiasContract;
import com.example.demo.entity.Noticias;
import com.example.demo.repository.INoticiasRepository;

@Service
public class NoticiasService implements INoticiasContract {

	@Autowired
	private INoticiasRepository noticiasRepository;
	
	@Override
	public List<Noticias> findAll() {
		// TODO Auto-generated method stub
		return (List<Noticias>)noticiasRepository.findAll();
	}

	@Override
	public Noticias save(Noticias noticias) {
		// TODO Auto-generated method stub
		return noticiasRepository.save(noticias);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		noticiasRepository.deleteById(id);
	}

	@Override
	public Noticias findByTextoAndTitulo(String texto, String titulo) {
		// TODO Auto-generated method stub
		return noticiasRepository.findByTextoAndTitulo(texto, titulo);
	}

	@Override
	public List<Noticias> findSection(int start, int end) {
		List<Noticias> noticiasDb = (List<Noticias>) noticiasRepository.findAll();
		if(noticiasDb.size() >= 20) {
			List<Noticias> noticiasReturn = new ArrayList<>();
			for(int i = end; i >= start; i--) {
				noticiasReturn.add(noticiasDb.get(i));
			}
			
			return noticiasReturn;
		}else {
			return noticiasDb;
		}
	}

}
