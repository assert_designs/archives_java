package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Noticias;
import com.example.demo.service.NoticiasService;
import com.google.gson.Gson;

import responses.Dates;
import responses.ResponsesErrorCreated;
import responses.ResponsesErrorEmpty;


@RestController
@CrossOrigin
@RequestMapping("/noticias")
public class NoticiasController {
	
	ResponsesErrorCreated errorObj = new ResponsesErrorCreated();
	ResponsesErrorEmpty errorEmptyObj = new ResponsesErrorEmpty();
	Dates okObj = new Dates();
	Gson gson = new Gson();
	String state;

	@Autowired
	private NoticiasService noticiasService;
	
	@GetMapping("/all_noticias")
	@ResponseStatus(HttpStatus.OK)
	public List<Noticias> getAllNoticias(){
		return noticiasService.findAll();
	}
	
	@PostMapping("/get/noticias")
	public ResponseEntity<?> loadNoticias(@RequestBody Noticias noticias){
		List<Noticias> getNoticias = noticiasService.findSection(noticias.getStart(), noticias.getEnd());
		if(!getNoticias.isEmpty() && getNoticias != null && noticias.getFecha() != null) {
			return new ResponseEntity<>(getNoticias, HttpStatus.OK);
		}else {
			state = gson.toJson(errorEmptyObj);
			return new ResponseEntity<>(state, HttpStatus.CONFLICT);
		}
	}
	
	@PostMapping("/post")
	public ResponseEntity<?> postNoticias(@RequestBody Noticias noticias){
		noticiasService.save(noticias);
		state = gson.toJson(okObj);
		return new ResponseEntity<>(state, HttpStatus.CREATED);
	}

}
