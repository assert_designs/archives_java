package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Citas;
import com.example.demo.service.CitasService;
import com.google.gson.Gson;

import responses.Dates;
import responses.ResponseAvailable;
import responses.ResponseNotAvailable;

@RestController
@CrossOrigin
@RequestMapping("/citas")
public class CitasController {

	//available
	ResponseAvailable avaObj = new ResponseAvailable();
	ResponseNotAvailable notAvaObj = new ResponseNotAvailable();
	Dates okObj = new Dates();
	Gson gson = new Gson();
	String state;
	
	@Autowired
	private CitasService citasService;
	
	@PostMapping("/comprobar_cita")
	public ResponseEntity<?> comporbarCita(@RequestBody Citas citas){
		Citas citaDb = citasService.findByTecnico(citas.getFecha(), citas.getHora(), citas.getTecnico());
		if(citaDb == null) {
			state = gson.toJson(avaObj);
			return new ResponseEntity<>(state, HttpStatus.OK);
		}else {
			state = gson.toJson(notAvaObj);
			return new ResponseEntity<>(state, HttpStatus.OK);
		}
	}
	
	@PostMapping("/asignar")
	public ResponseEntity<?> asignedCita(@RequestBody Citas citas){
		citasService.save(citas);
		state = gson.toJson(notAvaObj);
		return new ResponseEntity<>(state, HttpStatus.CREATED);
	}
}
