package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Usuario;
import com.example.demo.service.UsuarioService;
import com.google.gson.Gson;

import responses.Dates;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class UsuarioController {
	
	Dates obj = new Dates();
	Gson gson = new Gson();
	String created = gson.toJson(obj);
	
	@Autowired
	private UsuarioService _usuarioService;
	
	@PostMapping("/usuarios/login")
	public ResponseEntity<?> login(@RequestBody Usuario usuario) {
		Usuario u = _usuarioService.findByNombreEndsWith(usuario.getNombreUsuario(), usuario.getPasswordUsuario());
		if(u != null) {
			return new ResponseEntity<>(u, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/usuarios/register")
	public ResponseEntity<?> registerUser(@RequestBody Usuario usuario){
		if(_usuarioService.findByNombreAndPassword(usuario.getNombreUsuario(), usuario.getPasswordUsuario())
				== null){
			_usuarioService.save(usuario);
			return new ResponseEntity<>(created, HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
	
}
